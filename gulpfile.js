/**
 * Created by xiey on 2016/3/30.
 */
var del      = require('del'),
    gulp     = require('gulp'),
    $        = require('gulp-load-plugins')(),
    gulpSync = $.sync(gulp);

var PATH = {
    BUILD: './build',
    SRC  : ['./src/js/lib/**/*.js', './src/js/a.module.js', './src/js/core/**/*.js']
};

gulp.task('clean', function (done) {
    return del(PATH.BUILD, done);
});

gulp.task('minify', function () {
    return gulp.src(PATH.SRC)
        .pipe($.concat('a.core.js'))
        .pipe($.header('(function(window, angular, undefined) {\'use strict\';\n'))
        .pipe($.footer('\n}(window, angular));'))
        .pipe($.uglify())
        .pipe(gulp.dest(PATH.BUILD))
});

gulp.task('default', gulpSync.sync(['clean', 'minify']));