AModule.service('A', AService);

AService.$inject = [
    'A.Ajax',
    'A.Route',
    'A.PromiseTracker'
];

function AService(Ajax, Route, PromiseTracker) {
    this.Ajax = Ajax;
    this.Route = Route;
    this.PromiseTracker = PromiseTracker;
}