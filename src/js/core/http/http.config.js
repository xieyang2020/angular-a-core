AModule.config(['$httpProvider', function ($httpProvider) {

    var interceptor = ['$q', '$location', 'STATUS.DATA', function ($q, $location, STATUS) {

        /*ajax请求前配置*/
        //function request(config) {
        //    return config;
        //}

        //function requestError(rejection) {
        //
        //    $log.log(rejection);
        //    switch (rejection.status) {
        //        case HTTP_STATUS.UN_AUTHORIZED:
        //            $location.path('app/view/page/login.html');
        //            break;
        //        case HTTP_STATUS.FORBIDDEN:
        //            $location.path('app/view/page/login.html');
        //            break;
        //        default:
        //
        //    }
        //    return $q.reject(rejection);
        //}
        //
        //function response(response) {
        //    $log.debug('response -> ', response);
        //    return response;
        //}

        /*ajax请求完成配置*/
        function responseError(rejection) {

            switch (rejection.status) {
                case STATUS.UN_AUTHORIZED://不break 直接进入下一个case处理
                case STATUS.FORBIDDEN:
                    break;
                default:

            }
            return $q.reject(rejection);
        }

        return {
            //request: request,
            //requestError: requestError,
            //response: response,
            responseError: responseError
        };
    }];
    $httpProvider.interceptors.push(interceptor);
}]);