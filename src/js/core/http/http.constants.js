AModule.constant('STATUS.DATA', {
    'UN_AUTHORIZED': 401, // 未认证
    'FORBIDDEN': 403, // 无权访问
    'NOT_FOUND': 404
});
