/**
 * ========================================================= Module:
 * constants.js Define constants to inject across the application
 * =========================================================
 */
AModule.constant('STRINGS.DATA', {
        'NETWORK_ERROR': '网络错误',
        'LOGIN_ERROR': '登录失败'
    });