### 使用方法
    angular.toJson(obj, [pretty]);
    
### 说明
    obj 需要转换的对象
    pretty boolean值. 是否格式化(添加换行及空格)
    把 obj 转 json 字符串.
    