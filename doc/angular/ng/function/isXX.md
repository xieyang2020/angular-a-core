### 使用方法
    angular.isArray(value);
    angular.isDate(value);
    angular.isDefined(value);
    angular.isElement(value);
    angular.isFunction(value);
    angular.isNumber(value);
    angular.isObject(value);
    angular.isString(value);
    angular.isUndefined(value);
    
### 说明
    确认value是否是指定的类型值