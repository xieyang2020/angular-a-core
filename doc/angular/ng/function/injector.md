### 使用方法
    angular.injector(modules);
    
### 说明
    modules: 数组,需要 injector 的模块
    创建一个 injector 方法
    如: var $injector = angular.$injector(['ng']);
    $injector.get(..), $injector.invoke(..)(返回的injector使用请看 auto->service->$injector 说明)