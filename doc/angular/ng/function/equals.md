### 使用方法
    angular.equals(o1, o2);
    
### 说明
    确定2个值或对象是否相等, 支持值类型, 正则表达式, 数组及对象. 返回 true/false.