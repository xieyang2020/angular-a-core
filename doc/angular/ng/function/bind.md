### 使用方法
    angular.bind(self, fn, args);
    
### 说明
    返回 fn 方法, 且 fn 中 this 指向 self. args(数组)是传入的参数, 非必需(类似 fn.apply(self, args))