### 使用方法
    angular.element(element);
    
### 说明
    类似 jQuery 的 dom 选择器. 如: angular.element('.selector');
    如果在angular之前引入了 jQuery 则 angular.element 可以使用 jQuery 对象的全部方法, 否则只能使用 addClass(),after(),append(),attr(),bind(),children(),clone(),contents()等(详见 api).
    
    另外 angualr 还给返回的对象扩展了一些事件及方法:
        事件:
            $destroy 当 element 被移除会触发此事件
        
        方法:
            controller(name)返回当前或当前元素父级的 controller
            injector        返回当前或当前元素父级的 injector
            scope           返回当前或当前元素父级的 scope
            isolateScope    返回一个 isolate scope ,如果当前元素是isolateScope否则返回undefined
            inheritedData   返回element上所有的缓存数据, 像juqery的data()方法一样