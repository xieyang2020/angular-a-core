### 使用方法
    angular.extend(dst, src);
    
### 说明
    复制src中的属性到dst上, 如果存在相同的属性则覆盖dst而保存src的, 可以指定多个src对象. 如:
``` js
var dst, src1, src2;
dst = {name: 'Jim'};
src1 = {name: 'Jack', age: 24, address: 'China'};
src2 = {name: 'Tom', age: 25, job: 'IT'};
angular.extend(dst, src1, src2);
// dst => {name: 'Tom', age: 25, address: 'China', job: 'IT'}
// 也可以 dst = angular.extend({}, src1, src2);
```