### 使用方法
    angular.bootstrap(element, [modules]);
    
### 说明
    手动启动 angular 程序, 类似 ng-app 的作用. 如: angular.bootstrap(document, [..]);需要放到 js 最后面启动