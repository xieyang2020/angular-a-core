### 使用方法
    angular.copy(source, [destination]);
    
### 说明
    深度(完整)复制 source 对象, source可以是任何对象, 包括 primitives(原始值数字, 字符串等), null, undufine
    如:
``` js
var source,destination;
source = {name: 'Jim'};
destination = angular.copy(source);
// 或 angular.copy(source, destination); 他们的效果是一样的
```