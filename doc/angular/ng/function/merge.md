### 使用方法
    angular.merge(dst, src);
    
### 说明
    深度复制. 注意与extend的区别
    
``` js
var object1 = {
  "key": "abc123def456",
  "message": {
    "subject": "Has a Question",
    "from": "example1@example.com",
    "to": "example2@example.com"
   }
}, object2 = {
  "key": "00700916391",
  "message": {
    "subject": "Hey what's up?",
    "something": "something new"
   }
};
var obj1={}, obj2={};
angular.extend(obj1, object1, object2);
angular.merge(obj2, object1, object2);
// extend 只复制第一层
// obj1 => key: "00700916391",
//         message: { 
//             something: "something new",
//             subject: "Hey what's up?"
//         }

// merge 递归复制
// obj2 => key: "00700916391",
//         message: { 
//             from: "example1@example.com",
//             something: "something new",
//             subject: "Hey what's up?",
//             to: "example2@example.com"
//         }
```