### 使用方法
    angular.module(name, [requires], [configFn]);
    
### 说明
    name 创建或获取的模块名
    require 数组, 指定了requires表示创建的模块所需要依赖的模块
    configFn 配置函数, 相当于 XXmodule.config(configFn);
    
    模块的创建或获取, 大于2个参数时是创建, 只有一个参数时是获取