### 使用方法
    angular.forEach(obj, iterator, [context]);
    
### 说明
    obj 数组或对象
    iterator 迭代方法 arguments 分别是 value, key
    context 非必需参数, iterator函数中的 this 指向对象.
    此方法返回 value
    
``` js
var values = {name: 'misko', gender: 'male'}, log = [];
angular.forEach(values, function(value, key) {
    this.push(key + ': ' + value);
}, log);
// log => ['name: misko', 'gender: male']
```