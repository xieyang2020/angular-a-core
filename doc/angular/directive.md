    规范  以 ag-*

    [compile-vs-link](http://hellobug.github.io/blog/angularjs-directive-2-compile-vs-link/)
    [详解](http://blog.51yip.com/jsjquery/1607.html)
    [compile详解](http://www.jb51.net/article/58229.htm)

```js
angular.module("app.directive", []).directive('agExample', example);

example.$inject = [...];

function example(...) {

    var directive = {
        restrict: 'AE',
        priority: 100,//优先级, 调用顺序
        terminal: true,//是否停止运行当前元素上比本指令优先级低的指令, 但同当前指令 优先级相同的指令还是会被执(为false时停用)
        scope: {
            options: '=agOptions', 不确定父级是否存在时使用 =?attr 或 =? ; If you want to shallow watch for changes you can use =* or =*attr (=*? or =*?attr if the property is optional).
            type: '@type',
            someData: '&data'
        },
        require: [], // 可以是数组也可以是某一个 directive(也可以是自己), 在 link 的第四个参数可以引用directive的controller中的方法
        template: '<div class="example"></div>',
        transclude: true, // [true, 'element'] ; true: 编译后的html包裹在 ng-transclude 标签下; 'element': 写的什么标签就包裹在 ng-transclude 标签下, 如果写的 自定义directive那么就包裹在 ng-transclude 标签下 而不是编译好了的html
        replace: true,
        compile: compile,
        link: link, // 就是 postLink, 当有compile时link就没用了(compile中可以返回preLink和postLink)
        controller: controller
    };
    
    function compile(element, attrs, transclude) {
        // 在dom渲染前对它进行变形
        
        return {
            // 从父级到子级 (详见http://www.jb51.net/article/58229.htm)
            'pre': function(scope, element, attributes){
                
            },
            // 从子级到父级
            'post': function(){
            
            }
        };
    }
    
    function link(scope, element, attrs, controller) {
        //事件处理
    }
    
    function controller() {
        //业务逻辑之类
    }
    
    return directive;
}
```

```
执行顺序  html -> dom -> compile -> link -> controller
```

    restrict  EACM的子集的字符串, 如果省略的话，directive将仅仅允许通过属性声明：(E - 元素名称： <my-directive></my-directive>; A - 属性名： <div my-directive=”exp”></div>; C - class名： <div class=”my-directive:exp;”></div>; M - 注释 ： <!-- directive: my-directive exp -->)
    
    scope true/false/{..}
    
    require 可添加前缀 ?/^/?^.如果不添加表示从当前directive查找控制器(controller);^表示向上游的directive查找,但如果没查到会抛出一个错误;?表示如果在当前directive中未找到控制器就将null作为link的第四个参数;^?就是^和?结合起来的意思