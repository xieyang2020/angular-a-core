### 简介
    根据某用户是否含有某权限来显示/隐藏添加了ag-has-permission属性的元素
    
### 使用
``` html
<!-- 表示此button只有在用户有ADMIN权限是才显示出来 -->
<button ag-has-permission="ADMIN"></button>
```