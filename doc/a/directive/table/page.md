### 简介
    通过配置呈现一个个样式统一的表格
    
### 使用
``` html
<!-- ag-options 配置项 -->
<!-- ag-search-params 查询对象 -->
<ag-page-table ag-options="options" ag-search-params="params"></ag-page-table>
```

``` js
// 初始化 table查询参数(根据 ag-search-params 参数配置的)
$scope.params = {};

$scope.options = {
    api: Api.admin.user.page,//后台请求连接
    checkbox: true, //是否含有复选框
    column: [{
        name: '序号',
        index: true // true 显示1.2.3的序号
    }, {
        name: '姓名',
        index: 'name' // 显示获取数据中对应为name的值
    }, {
        name: '状态',
        index: 'state',
        renderer: function (value) { // 转义. 把 value 根据业务判断转换为 需要的值(注: 不支持返回 html 标签内容, angular是html安全的)
            if (value.state == 1) {
                return "有效";
            }
            return "无效";
        }
    }, {
        name: '更新时间',
        index: 'updatime',
        //format: 'yyyy:MM:dd HH:mm:ss',
        type: 'date' // date -> yyyy-MM-dd; datetime -> yyyy-MM-dd HH:mm:ss; 自定义: 添加format属性自定义格式
    }, {
        name: '操作列',
        type: 'action', // 操作列
        items: [{
            icon: 'fa fa-edit', // 图标样式
            tooltip: '修改', //鼠标悬浮显示内容
            handler: function (o, row) { //点击事件处理, o: 当前table的Scope, row: 点击列的数据
                $state.go('admin.user.save', {
                    id: row.id
                });
            }
        }, ...]
    }]
};
```