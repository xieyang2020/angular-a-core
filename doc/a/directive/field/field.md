### 简介
    对 form 表单中的 field 进行了简单的包装, 减少代码量的编写,使 form 表单风格一致
    
### 使用
        <div ag-field ag-label="姓名" ag-options="{required:true}" ag-name="name" ag-modle="user.name"
             ag-form="userForm">
            <span ng-message="required">{{errors.name}}</span>
        </div>
        
        ag-field 声明, 可以是属性或标签
        ag-label label名称
        ag-options 
        ag-name input的name属性
        ag-model 与ng-model一样
        ag-form form表单名称(form表单的name值)
        div中的span是每个错误(ng-message)对应的消息提示