### 简介
    根据 UI Bootstrap 的 Datepicker 进行的简单包装
    
### 使用
    <div ag-date-picker ag-model="date.time" ag-min-date="date.min" ag-max-date="date.max"></div>
    
    ag-date-picker 可以作为属性或标签使用
    ag-model 就是 ng-model 的用法
    ag-min-date 最小能选择的时间
    ag-max-date 最大能选择的时间
    ag-name 就是input的name属性