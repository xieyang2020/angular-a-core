### 简介
    对 ui-select 的简单包装
    
### 使用
    <div class="input-group" ag-select ag-model="role.three" ag-data="data.three" ag-change="orgChange($select.selected, 'last')"></div>
    
    ag-select 声明, 可以是标签或属性
    ag-model ng-model一样的用法
    ag-data select标签显示的数据
    ag-display 下拉显示的值, 默认是 name
    ag-placeholder 未选择时显示的提示语, 默认是 请选择..
    ag-change select的change事件
    
    $select.selected 表示当前下拉框选中的对象