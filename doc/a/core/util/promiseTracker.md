### 简介 
   监控 promise 的进度情况(是否完成, 是否正在处理中)
   
### 使用
``` js
// 1. 注入 promiseTracker
LoginFormController.$inject = ['promiseTracker',..];
// 2. 初始化监控器(这里假设注入名就是promiseTracker)
$scope.progress = promiseTracker();
// 3. 添加需要监控的 promise
var promise = $http.get(..).then(..);
$scope.progress.addPromise(promise);
// 4. 使用 promiseTracker 的方法
$scope.progress.active(); // 判断 promise 是否完成
$scope.progress.tracking(); // 判断 promise 是否正在加载中
```